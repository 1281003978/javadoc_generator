package com.hongcheng.javadoc_generator;

/**
 * 	可见性修饰符
 * */
public enum Modifier{
	PUBLIC,PROTECTED,PRIVATE;
}